#!/usr/bin/env python3

import argparse
import logging
import sys
from argparse import RawTextHelpFormatter

import pmgridtools.gridtools as gridtools
import pmgridtools.jobhandling as jobhandling

viewnames = [None, "wait_to_stage", "to_stage", "staging", "todo", "locked", "error"]


def main():
    logging.basicConfig()
    parser = argparse.ArgumentParser(
        description="""delete tokens in selected views
examples:

delete a token with specific token id
%(prog)s HG005146a62ba81

delete multiple tokens with specific token ids
%(prog)s HG005146a62ba81 LP6008934_F06a62ba81

remove all tokens with certain id from one specific view
%(prog)s -s HG005146 -1

remove all tokens with certain id from multiple views
%(prog)s -s HG005146 -123456

remove all tokens based on fingerprint from multiple views
%(prog)s -f a62ba81 -123456


        """,
        formatter_class=RawTextHelpFormatter,
    )

    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "-f",
        "--fingerprint",
        dest="fingerprint",
        help="specify the adler32 fingerprint to delete",
        type=str,
        default=None,
    )
    group.add_argument(
        "-s",
        "--sample",
        dest="sample",
        help="specify the sample id to delete",
        type=str,
        default=None,
    )

    parser.add_argument(
        "-1",
        action="store_true",
        help=f"select {viewnames[1]} to search for sampleid or fingerprint",
    )
    parser.add_argument(
        "-2",
        action="store_true",
        help=f"select {viewnames[2]} to search for sampleid or fingerprint",
    )
    parser.add_argument(
        "-3",
        action="store_true",
        help=f"select {viewnames[3]} to search for sampleid or fingerprint",
    )
    parser.add_argument(
        "-4",
        action="store_true",
        help=f"select {viewnames[4]} to search for sampleid or fingerprint",
    )
    parser.add_argument(
        "-5",
        action="store_true",
        help=f"select {viewnames[5]} to search for sampleid or fingerprint",
    )
    parser.add_argument(
        "-6",
        action="store_true",
        help=f"select {viewnames[6]} to search for sampleid or fingerprint",
    )

    parser.add_argument(
        "-n",
        "--dryrun",
        action="store_true",
        help="perform all actions except actual delete",
    )

    parser.add_argument("tokenids", nargs=argparse.REMAINDER)
    parse_results = parser.parse_args()

    # print help when no args are given
    if len(sys.argv[1:]) == 0:
        parser.print_help()
        parser.exit()

    view_to_process = [
        viewnames[numeric_option]
        for numeric_option in range(1, len(viewnames))
        if vars(parse_results)[str(numeric_option)]
    ]

    db = gridtools.connect_to_couchdb()

    stopjobs = jobhandling.StopRunningJobs()
    if len(parse_results.tokenids) > 0:
        if view_to_process:
            print_and_exit_status1(
                "can not select views (-1,-2 etc) when tokenid is selected",
                parse_results,
            )

        if parse_results.fingerprint is None and parse_results.sample is None:
            ddocs = _extracted_from_main_42(parse_results, db, stopjobs)
        else:
            print_and_exit_status1(
                "can not use -s or -f in combination with removing tokenids",
                parse_results,
            )

    if parse_results.fingerprint is not None and len(parse_results.fingerprint) != 8:
        print("ERROR: fingerprint given has not the length of 8 characters")
        exit(1)

    ddocs = []
    for viewname in view_to_process:
        for row in db.iterview(f"generic/{viewname}", 200, reduce=False):
            if (
                parse_results.fingerprint is not None
                and row["value"]["fingerprint"] == parse_results.fingerprint
            ):
                ddocs.append({"_id": row["id"], "_rev": row["value"]["_rev"]})
                stopjobs.addjobfromtoken(row["value"])
            if (
                parse_results.sample is not None
                and row["value"]["sampleid"] == parse_results.sample
            ):
                ddocs.append({"_id": row["id"], "_rev": row["value"]["_rev"]})
                stopjobs.addjobfromtoken(row["value"])
    print(f"found {len(ddocs)} tokens")
    if not parse_results.dryrun:
        db.purge(ddocs)
    stopjobs.stop_jobs()


# TODO Rename this here and in `main`
def _extracted_from_main_42(parse_results, db, stopjobs):
    result = []
    for tid in parse_results.tokenids:
        try:
            doc = db[tid]
            result.append({"_id": doc.id, "_rev": doc.rev})
            stopjobs.addjobfromtoken(doc)
        except gridtools.couchdb.http.ResourceNotFound:
            print(f"could not find {tid} in the database")
    if not parse_results.dryrun:
        db.purge(result)
    stopjobs.stop_jobs()
    print(f"found {len(result)} tokens")

    exit(0)

    return result


def print_and_exit_status1(arg0, parse_results):
    print(arg0)
    print(parse_results)
    exit(1)


if __name__ == "__main__":
    main()
