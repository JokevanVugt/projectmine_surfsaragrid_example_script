#!/usr/bin/env python3

import argparse
import os
import re
from shutil import copyfile

import pmgridtools.gridtoolslite as gridtoolslite
import pmgridtools.webdav_dcache as webdav


class Gridpath:
    def __init__(self, url):
        self.orig = url
        url = str(url).strip()
        self.pnfs = None
        self.local = None
        if url.startswith("gsiftp://") or url.startswith("srm://"):
            self.pnfs = re.sub(r".*/pnfs/grid.sara.nl/", "/pnfs/grid.sara.nl/", url)

        else:
            url = os.path.abspath(url)
            # print(url)
            if url.startswith("/projectmine-nfs/"):
                self.pnfs = url.replace(
                    "/projectmine-nfs/", "/pnfs/grid.sara.nl/data/lsgrid/Project_MinE/"
                )
            else:
                self.local = url

    def gridftpformat(self):
        if self.pnfs:
            return f"gsiftp://gridftp.grid.sara.nl:2811{self.pnfs}"
        return f"file:///{self.local}"

    def srmformat(self):
        if self.pnfs:
            return f"srm://srm.grid.sara.nl:8443{self.pnfs}"
        return f"file://+{self.local}"

    def localformat(self):
        if self.local:
            return self.pnfs
        else:
            raise Exception("File is not local")

    def online(self):
        if self.pnfs:
            ctx = webdav.WebDav()
            status = ctx.locality(self.webdavformat())
            return status in ("ONLINE_AND_NEARLINE", "ONLINE")
        return True

    def adler32(self):
        if not self.pnfs:
            return gridtoolslite.adler32_of_file(self.local)

        ctx = webdav.WebDav()
        return ctx.adler32(self.webdavformat())

    def webdavformat(self):
        """
        Convert a turl/surl to a webdav url
        """
        if self.pnfs:
            return f"https://webdav.grid.surfsara.nl:2884{self.pnfs}"
        else:
            raise Exception(f"Failed to create a webdav url{self.orig}")

    def exists(self):
        if not self.pnfs:
            return os.path.exists(self.local)

        wd = webdav.WebDav()
        return wd.exists(self.webdavformat())

    def remove(self):
        if self.pnfs:
            wd = webdav.WebDav()
            wd.remove(self.webdavformat())
        else:
            os.remove(self.local)

    def localfile(self):
        return bool(self.local)

    def needs_file_name_from_source(self):
        return bool(
            self.orig == "." or self.orig.endswith(".") or self.orig.endswith("/")
        )

    def basename(self):
        if self.pnfs:
            return os.path.basename(self.pnfs)
        return os.path.basename(self.local)

    def add_filename_to_dir(self, filename):
        if self.pnfs:
            self.pnfs = f"{self.pnfs}/{filename}"
        else:
            self.local = f"{self.local}/{filename}"


def copy(src_obj, dest_obj):
    if src_obj.localfile() and dest_obj.localfile():
        copyfile(src_obj, dest_obj)
    else:
        cmd = [
            "globus-url-copy",
            "-create-dest",
            "-rst",
            src_obj.gridftpformat(),
            dest_obj.gridftpformat(),
        ]
        gridtoolslite.execute(cmd, timing=False)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="copy files from/to grid storage")

    parser.add_argument("src", metavar="src", type=str, help="source file")
    parser.add_argument(
        "dest",
        metavar="dest",
        type=str,
        help="target file: this may be a '.'  to use the same filename as the source",
    )

    # get full paths
    args = parser.parse_args()

    dest = Gridpath(args.dest)
    src = Gridpath(args.src)

    # if dest end with a dot or an "/" make a postfix that is equal to the basename of dest
    if dest.needs_file_name_from_source():
        dest.add_filename_to_dir(src.basename())

    if dest.exists():
        srcadler = src.adler32()
        destadler = dest.adler32()
        if srcadler == destadler:
            print(
                f"destination exist: {src.orig} and {dest.orig} have the same adler32 ({destadler})"
            )
            exit(0)
        else:
            print(
                f"destination exist: {src.orig}({srcadler}) and {dest.orig} ({destadler}) are not the same"
            )
            exit(1)

    srcadler = None
    # copy file
    if src.online():
        srcadler = src.adler32()
        copy(src, dest)

    else:
        print("source file is not online:coping failed")
        exit(1)
    # get adler32 of target
    destadler = dest.adler32()

    if destadler != srcadler:
        dest.remove()
        print("adler do not match destination removed")
        exit(1)
