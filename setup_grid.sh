#!/bin/bash

set -x
set -e

export PATH=/cvmfs/softdrive.nl/projectmine_sw/software/bin:$PATH
mail="helpdesk@surfsara.nl"

#unset java options
unset _JAVA_OPTIONS

freespace=`stat --format "%a*%s/1024^3" -f $TMPDIR|bc`
echo $freespace

df $TMPDIR
if [ $freespace -gt 120 ]; then  echo "enough free space in scratch: $freespace GB" ; else echo "not enough free space in scratch: $freespace GB in TMPDIR=$TMPDIR"|  mail -s "[grid] Not enough diskspace found at $HOSTNAME in TMPDIR=$TMPDIR" $mail; exit 1; fi

#fix binding dir with symlinks etc
tmp_singularity=`echo $TMPDIR|sed -e 's@^/data/@/@'`
if [ -d "$tmp_singularity" ]; then
    export TMPDIR=${tmp_singularity}
fi

# test if softdrive is present
if [ -d /cvmfs/softdrive.nl/projectmine_sw ]
  then
    echo "Softdrive found"
 else
        echo "softdrive not found"
        echo "There is no softdrive found on $HOSTNAME" | mail -s "[grid] No softdrive found at $HOSTNAME" $mail
        exit 1
fi

whoamigrid="curl --capath /etc/grid-security/certificates --cert "$X509_USER_PROXY" --cacert "$X509_USER_PROXY"  --fail --silent --show-error --ipv4 -X GET https://dcacheview.grid.surfsara.nl:22882/api/v1/user"
if $whoamigrid ;
then
    echo "able to login to dcache
 else
        echo "User not able to login to dcache"
        $whoamigrid
        echo "User not able to login to dcache $HOSTNAME" | mail -s "[grid] User not able to login to dcache  $HOSTNAME with dirac id $DIRACJOBID" maarten@oyat.nl
        voms-proxy-info --all
        exit 1
fi



export REF_PATH=/cvmfs/softdrive.nl/projectmine_sw/resources/refpath/cache/%2s/%2s/%s:http://www.ebi.ac.uk/ena/cram/md5/%s
#export REF_CACHE=/cvmfs/softdrive.nl/projectmine_sw/resources/refpath/cache/%2s/%2s/%s

sleep $(awk  -v seed="$RANDOM" 'BEGIN { srand(seed); printf("%.4f\n", rand()*10) }')
#add current dir to python path to include credenials.py (needed for pm_copy)
export PYTHONPATH=$PYTHONPATH:$PWD
python /cvmfs/softdrive.nl/projectmine_sw/software/pop/gridjob_new.py -t $1 -m $2
