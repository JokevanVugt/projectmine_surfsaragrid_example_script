#GRID filter=.*.tar.gz
#GRID output=/projectmine-nfs/Disk/User/kooyman/
ExpansionHunter --bam $bam_path --ref-fasta /cvmfs/softdrive.nl/maartenk/project_mine/reference/ucsc.hg19_fixed_maarten.fasta --repeat-specs /cvmfs/softdrive.nl/jvanvugt/RepeatHunter/JSON_targets  --skip-unaligned yes --vcf $sample"_ExpansionHunter.vcf" --json $sample"_ExpansionHunter.json" --log $sample"_ExpansionHunter.log"
#touch $sample"_ExpansionHunter.vcf" $sample"_ExpansionHunter.log" $sample"_ExpansionHunter.json"
tar -czvf $sample.tar.gz $sample"_ExpansionHunter.vcf" $sample"_ExpansionHunter.log" $sample"_ExpansionHunter.json"
