import inspect
import logging
import os
import re
import shutil
import sys
from os.path import exists
from subprocess import PIPE, Popen
from time import time
from typing import Dict

import couchdb
from picas.actors import RunActor

import pmgridtools.gridtoolslite as gridtoolslite
import pmgridtools.webdav_dcache as webdav_dcache

sys.path.append(os.getcwd())

import credentials  # noqa: E402

logger = logging.getLogger("mylogger")


class CouchDBLogHandler(logging.StreamHandler, object):
    def __init__(self, client, token):

        super(CouchDBLogHandler, self).__init__()
        self.client = client
        self.token = token

    def format(self, record):

        """ """
        msg = str(record.msg)
        # make log smaller when they are huge
        if len(msg) > 25000:
            msg = (
                msg[:5000] + "\n\nTruncated due large log entry size \n\n" + msg[-5000:]
            )

        return dict(
            message=msg,
            level=record.levelname,
            created=int(record.created),
            line=record.lineno,
        )

    def emit(self, record):
        """ """
        if self.token:
            # token = self.client.db[self.tokenid]
            if "log" not in self.token:
                self.token["log"] = [self.format(record)]
            else:
                self.token["log"].append(self.format(record))

            self.token["_rev"] = self.client.db.save(self.token.value)[1]

        else:
            self.client.db.save(self.format(record))


class MineRunner(RunActor):
    """ """

    def __init__(self, iterator, modifier):
        """ """
        # RunActor.__init__(iterator, modifier)
        # This is what happens in RunActor:
        self.iterator = iterator
        self.modifier = modifier
        self.token = None
        self.token_rev = None
        # Make a copy of the db reference
        self.client = iterator.database
        self.db = iterator.database
        self.tasks_processed = 0

    def download_all_files(self, files_remote_map):
        """ """
        filemap = {}
        for filetype in files_remote_map:
            remotefile = str(files_remote_map[filetype]["url"])
            basename_remote = os.path.basename(remotefile)
            adler_hash = None
            md5_hash = None
            if "adler32" in files_remote_map[filetype]:
                adler_hash = str(files_remote_map[filetype]["adler32"])
            if "md5" in files_remote_map[filetype]:
                md5_hash = str(files_remote_map[filetype]["md5"])
            localfile = os.environ["PWD"] + "/" + basename_remote
            download_file(
                remotefile, localfile, adler32Hash=adler_hash, md5Hash=md5_hash
            )
            filemap[filetype] = localfile

        return filemap

    def execute(
        self, args, shell=False, logname_extention="", stdout_fh=PIPE, timing=True
    ):
        """Helper function to more easily execute applications.
         @param args: the arguments as they need to be specified for Popen.
        @return: a dictonary containing the command,duration,exitcode, stdout & stderr
         :param shell:
         :param logname_extention:
         :param stdout_fh:

        """

        if isinstance(args, list):
            logger.debug(" ".join(args))
        else:
            logger.debug(args)
        # convert unicode strings to normal strings
        args = [str(s) for s in args]
        timingfile = ".gridtimings.tmp"
        if timing:
            args = ["/bin/time", "-o", timingfile] + args
        start = time()
        logger.debug(args)
        try:
            proc = Popen(args, stdout=stdout_fh, stderr=PIPE, shell=shell)
        except OSError as e:

            logger.error(str(e))

            logger.error(args)
            exit(1)
        except Exception as e:
            e = sys.exc_info()[0]
            print(e)
            logger.error("Failed execute")
            logger.error(args)
            exit(1)

        (stdout, stderr) = proc.communicate()
        stdout = str(stdout)
        stderr = str(stderr)
        end = time()
        # round duration to two floating points
        duration = int((end - start) * 100) / 100.0

        timingstats = None
        if timing:
            try:
                timingstats = open(timingfile).readlines()
                os.remove(timingfile)
            except FileNotFoundError:
                logger.warning("/bin/time file not found")

        if proc.returncode != 0:
            logger.error(f"exit code not 0: {proc.returncode}")
            logger.error(" ".join(args))
            logger.error(f"stdout:{stdout}")
            logger.error(f"stderr:{stderr}")

        job_report = {
            "command": " ".join(args),
            "exitcode": proc.returncode,
            "stdout": (
                f"{stdout[:100000]}/n/n/n..removed part of message due to large message./n/n/n{stdout[-100000:]}"
            )
            if len(stdout) > 250000
            else stdout,
            "stderr": (
                f"{stderr[:100000]}/n/n/n..removed part of message due to large message./n/n/n{stderr[-100000:]}"
            )
            if len(stderr) > 250000
            else stderr,
            "duration": duration,
            "timing": timingstats,
        }

        f = inspect.currentframe().f_back
        functname = f.f_code.co_name
        lineno = f.f_lineno
        atachment_name = f"{functname}_{lineno}{logname_extention}"

        self.client.db.put_attachment(self.token.value, str(job_report), atachment_name)
        self.token.update(self.client.db[self.token["_id"]])

        if proc.returncode != 0:
            exit(1)
        return job_report

    def worker(self, token):
        pass

    def prepare_env(self, *kargs, **kvargs):
        pass

    def prepare_run(self, *kargs, **kvargs):
        pass

    def process_task(self, token):
        # this is where all the work gets done. Start editing here.
        self.token = token

        # add new handler which logs to token
        logger.addHandler(CouchDBLogHandler(self.client, token))

        if "TMPDIR" not in globals():
            TMPDIR = os.environ["TMPDIR"] + "/"

        # create a new working enviroment
        working_dir = TMPDIR + str(token["_id"])
        os.mkdir(working_dir)
        os.chdir(working_dir)
        os.environ["PWD"] = working_dir
        job_report = self.worker(token)

        # remove all local files
        os.chdir(TMPDIR)
        os.environ["PWD"] = TMPDIR
        try:
            shutil.rmtree(working_dir)
        except FileNotFoundError:
            print(f"could not remove working dir {working_dir}. continue to next job")
        if job_report["timing"] is not None:
            token["timing"] = job_report["timing"]
        self.modifier.close(token)
        # remove old couchdbloggers to prevent that following task also log to previous token
        [
            logger.removeHandler(h)
            for h in logger.handlers
            if isinstance(h, CouchDBLogHandler)
        ]

    def cleanup_run(self, *kargs, **kvargs):
        pass

    def cleanup_env(self, *kargs, **kvargs):
        pass


def execute(args, shell=False, ignore_returncode=False, timing=True):
    """Helper function to more easily execute applications.
    @param shell: the arguments as they need to be specified for Popen.
    @return: a tuple containing the exitcode, stdout & stderr
    :param ignore_returncode:
    :param timing: time program with /bin/time
    :param shell:

    """

    if isinstance(args, list):
        logger.debug(" ".join(args))
    else:
        logger.debug(args)
    timingfile = ".gridtimings.tmp"
    if timing:
        timingcode = f"/bin/time -o {timingfile}"
        if isinstance(args, list):
            args = timingcode.split() + args
        else:
            args = f"{timingcode} {args}"

    try:
        proc = Popen(args, stdout=PIPE, stderr=PIPE, shell=shell)
    except OSError as e:
        logger.critical(e)
        logger.critical(args)
        exit(1)
    except Exception as e:
        e = sys.exc_info()[0]
        print(e)
        logger.error("Failed execute")
        logger.error(args)
        exit(1)
    (stdout, stderr) = proc.communicate()
    # make sure that type is not a number
    if type(stdout) == bytes:
        stdout = stdout.decode("utf-8")
    else:
        str(stdout)

    if type(stderr) == bytes:
        stderr = stderr.decode("utf-8")
    else:
        str(stderr)
    timingstats = None
    if timing:
        try:
            timingstats = open(timingfile).readlines()
            os.remove(timingfile)
        except FileNotFoundError:
            logger.warning("/usr/bin/time output file not found")

    if not ignore_returncode and proc.returncode != 0:
        logger.error(f"exit code not 0: {proc.returncode}")
        logger.error(" ".join(args))
        logger.error(f"stdout:{stdout}")
        logger.error(f"stderr:{stderr}")

        sys.exit(1)
    return {
        "exitcode": proc.returncode,
        "stdout": stdout,
        "stderr": stderr,
        "timing": timingstats,
    }


def add_variables_to_bash(
    template: str, required: Dict[str, str], optional: Dict[str, str]
) -> str:
    """

    :param template:
    :param required:
    :param optional:
    :return:
    """
    bash_prefix = ""
    # check if required fields are found
    for k, v in required.items():
        if template.find(f"${k}") != -1 or template.find(f"${{{k}}}") != -1:
            bash_prefix = f"{bash_prefix}\n{k}={v}"
        else:
            raise KeyError(f"Could not find ${k} or ${{{k}}} that is required")

    for k, v in optional.items():
        if template.find("$" + k) != -1 or template.find("${" + k + "}") != -1:
            bash_prefix = f"{bash_prefix}\n{k}={v}"

    return f"{bash_prefix}\n{template}"


def run_as_bash(commands):
    lastfunction = inspect.currentframe().f_back
    functname = str(lastfunction.f_code.co_name)
    if functname == "<module>":
        functname = "line"
    lineno = str(lastfunction.f_lineno)
    filename = f"{functname}_{lineno}.sh"

    with open(filename, "w") as fh:
        bashheader = """#!/bin/bash
set -x
set -u
set -e
set -o pipefail
"""

        fh.write(bashheader)
        fh.write(commands)
        # delete the script after running
        fh.write("\n rm -f " + filename)

    logger.debug(fh.closed)

    return ["/bin/bash", filename]


def md5_of_file(filepath):
    """
    Calculate md5 checksum of filepath
    """
    return gridtoolslite.md5_of_file(filepath)


def adler32_of_file(filepath):
    """
    Calculate adler32 checksum of filepath

    """
    return gridtoolslite.adler32_of_file(filepath)


def connect_to_couchdb(
    url=credentials.URL,
    username=credentials.USERNAME,
    password=credentials.PASS,
    dbname=credentials.DBNAME,
):
    """
    returns a object couchdb (not truly connect since couchdb is RESTFULL and no separate connection is made for authorization and authentication)
    """
    db = couchdb.Database(f"{url}/{dbname}")
    db.resource.credentials = (username, password)
    return db


def get_adler32_on_srm(remote_surl):
    """
    retrive adler32 hash via the srmls tools
    """
    ctx = webdav_dcache.WebDav()
    return ctx.adler32(convert_to_webdav(remote_surl))


def convert_to_surl(url):
    """
    Convert a turl/surl to a tupple of (surl,turl)
    """
    surl = None
    turl = None
    if str(url).startswith("srm://"):
        surl = url
        turl = re.sub(
            r"srm://srm.grid.sara.nl(:8443)?/",
            "gsiftp://gridftp.grid.sara.nl:2811/",
            url,
        )
    elif str(url).startswith("gsiftp://"):
        turl = url
        surl = re.sub(
            r"gsiftp://gridftp.grid.sara.nl(:2811)?/", "srm://srm.grid.sara.nl/", url
        )
    # url.replace("gsiftp://gridftp.grid.sara.nl:2811",
    # "srm://srm.grid.sara.nl", 1)
    else:
        logger.warning(
            "Downloading failed: url did not start with gsiftp:// or srm:// :"
            + str(url)
        )
    return surl, turl


def convert_to_webdav(url):
    """
    Convert a turl/surl to a webdav url
    """

    return gridtoolslite.convert_to_webdav(url)


def is_local_file(url):
    """
    Return true if url is existing local file and false if the file surl/turl

    exit if file does not exists localy
    """
    local_file = False
    if url.startswith("srm://"):
        local_file = False
    elif url.startswith("gsiftp://"):
        local_file = False
    elif os.path.isfile(url):
        local_file = True
    else:
        logger.error(f"{url} could not be identified as SURL TURL or localfile")
        exit(1)

    return local_file


def download_all_files(files_remote_map):
    """ """
    filemap = {}
    for filetype in files_remote_map:
        remotefile = str(files_remote_map[filetype]["url"])
        basename_remote = os.path.basename(remotefile)
        adler_hash = None
        md5_hash = None
        if "adler32" in files_remote_map[filetype]:
            adler_hash = str(files_remote_map[filetype]["adler32"])
        if "md5" in files_remote_map[filetype]:
            md5_hash = str(files_remote_map[filetype]["md5"])
        localfile = os.environ["PWD"] + "/" + basename_remote
        download_file(remotefile, localfile, adler32Hash=adler_hash, md5Hash=md5_hash)
        filemap[filetype] = localfile

    return filemap


def download_file(url, localfile, adler32Hash=None, md5Hash=None):
    """
    Download a file
    @url a srm, turl or local path
        - srm prefix is srm://
        - turl prefix is gsiftp://
        - local path has no prefix
    @localfile destignation of file
    @adler32Hash adler32 hash to verify the copied file
    @md5Hash md5 hash to verify the copied file
    """
    # verify location with srm
    # logger=logging.getLogger(__name__)

    local_copy = is_local_file(url)

    surl, turl = convert_to_surl(url)
    # srmcopy

    if not local_copy:
        starttime = time()
        adler_srm = get_adler32_on_srm(surl)
        if adler32Hash is not None and adler_srm != adler32Hash:
            logger.exception(
                f"Provided adler32 not the same as on SRM adler32 on server:{adler_srm} adler32 on server:{adler32Hash}"
            )
            exit(1)

        srmtime = time()
        # download file with globus-url-copy
        gridftp_localfile = localfile
        if not gridftp_localfile.startswith("file://"):
            gridftp_localfile = f"file://{localfile}"

        # TODO nikhef <-> Dcache does have network problem. until there is a solid fix we need a workaround
        hostname = os.uname()[1]
        cmd = None
        if "nikhef" in hostname or "spider" in hostname:
            cmd = [
                "globus-url-copy",
                "-rst-retries",
                "2",
                "-stall-timeout",
                "120",
                turl,
                gridftp_localfile,
            ]
        else:
            cmd = [
                "globus-url-copy",
                "-p",
                "4",
                "-rst-retries",
                "2",
                "-stall-timeout",
                "120",
                turl,
                gridftp_localfile,
            ]
        job_report = execute(cmd, ignore_returncode=True)

        globus_copytime = time()
        logger.debug(job_report)

        if job_report["exitcode"] != 0:
            ctx = webdav_dcache.WebDav()
            webdavurl = convert_to_webdav(surl)
            locality = ctx.locality(webdavurl)

            if (
                locality.startswith("ONLINE")
                and job_report["stderr"].strip()
                == "error: globus_ftp_client: the operation was aborted"
            ):
                logger.info(
                    "gridftp failed and file is online: using webdav as fallback"
                )
                ctx.download(webdavurl, localfile)

            else:

                logger.error("failed")
                logger.error(
                    "gridftp failed exit code not 0: " + str(job_report["exitcode"])
                )
                logger.error(" ".join(cmd))
                logger.error("stdout:" + job_report["stdout"])
                logger.error("stderr:" + job_report["stderr"])
                logger.error(f"locality of {surl}:{locality}")
                exit(1)
        webdav_copy_time = time()

    else:
        # copy on local files system
        cmd = ["cp", url, localfile]
        execute(cmd)
    # verify with adler 32 if file is the same
    if adler32Hash is not None:
        adler32_local = adler32_of_file(localfile)
        adlertime = time()
        srmtimenetto = srmtime - starttime
        globustimenetto = globus_copytime - srmtime
        webdavtimenetto = webdav_copy_time - globus_copytime
        adlertimenetto = adlertime - webdav_copy_time
        logger.info(
            "srm:{0:.2f}s gsiftp:{1:.2f}s adler:{2:.2f}s webdav:{3:.2f}s".format(
                srmtimenetto, globustimenetto, adlertimenetto, webdavtimenetto
            )
        )
        if adler32_local != adler32Hash:
            logger.exception(
                f"Incorrect adler32 hash for localfile {localfile} ({adler32_local}) downloaded from {url} ({adler32Hash})"
            )

            exit(1)
    # verify with adler 32 if file is the same
    if md5Hash is not None:
        md5_local = md5_of_file(localfile)
        if md5_local != md5Hash:
            logger.exception(
                f"Incorrect md5 hash for localfile {localfile} ({md5_local}) downloaded from {url} ({md5Hash})"
            )

            exit(1)


def rm_file(url):
    """
    remove a file independed on of surl/turl or local file (using local rm of globus-url-copy for gridftp)
    """

    if is_local_file(url):
        # copy on local files system
        cmd = ["rm", "-f", url]
    else:
        cmd = [
            "curl",
            "--capath",
            "/etc/grid-security/certificates/",
            "--cert",
            os.environ["X509_USER_PROXY"],
            "--cacert",
            os.environ["X509_USER_PROXY"],
            "--location",
            "--request ",
            "DELETE",
            convert_to_webdav(url),
        ]
    execute(cmd)


# def rm_dir(url):
#     """
#     remove a file depending on url which method(using local rm or gfal2 for gridftp)
#     """
#     local_copy = is_local_file(url)
#
#     surl, turl = convert_to_surl(url)
#
#     if not local_copy:
#         try:
#             context = gfal2.creat_context()
#             context.rmdir(turl)
#
#         except gfal2.GError as e:
#             logger.debug(str(e))
#     else:
#         # copy on local files system
#         cmd = ["rmdir", url]
#         job_report = execute(cmd)


def upload_file(local_file, remote_surl):
    """
    uploads a file to grid storage or localdisk
    when upload to gridftp a adler32 hash is calculated localy and compared with the adler32 in srm system
    """

    logger = logging.getLogger(__name__)

    # check localfile exisits
    if exists(local_file) is False:
        print("does not exists during upload")
        logging.error(f"{local_file}does not exist during upload to{remote_surl}")
        logger.error(f"{local_file}does not exist during upload to{remote_surl}")
        exit()

    # get local adler32
    adler32_local = adler32_of_file(local_file)

    if not is_local_file(remote_surl):
        # format the surl alright
        surl, turl = convert_to_surl(remote_surl)

        cmd = ["globus-url-copy", "-create-dest", "-rst", "-p", "4", local_file, turl]
        print("pre command")
        job_report = execute(cmd)
        print("logger.debug(job_report)")
        logger.debug(job_report)
        print("post logger.debug(job_report)")
        # get remote adler32
        adler_srm = get_adler32_on_srm(surl)
        # check id they are sam
        if adler32_local != adler_srm:
            logger.error(
                f"remote and local adler32 are unequal after upload of localfile {local_file} and remote file {remote_surl}"
            )
            rm_file(surl)
            exit(1)
    else:
        # logger.debug("using local copy from "+ local_file+" to "+ remote_surl)
        # logger.debug("creating dir: "+os.path.dirname(remote_surl))
        cmd = ["mkdir", "-p", os.path.dirname(remote_surl)]
        execute(cmd)
        cmd = ["cp", local_file, remote_surl]
        execute(cmd)

    return adler32_local


def validproxy():
    """

    :return: checks if proxy if valid. If not quit program and print help message
    """
    gridtoolslite.validproxy()
