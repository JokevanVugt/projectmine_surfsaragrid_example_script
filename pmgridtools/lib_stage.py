import logging
import re
from random import shuffle
from time import time

import gfal2

import pmgridtools.gridtools as gridtools
import pmgridtools.gridtoolslite as gridtoolslite
import pmgridtools.webdav_dcache as webdav_dcache


def n_files_online(surls):
    """
    Get the number of files back that are staged
    """
    # logger = logging.getLogger(__name__)

    surls = [gridtools.convert_to_surl(turl)[0] for turl in surls]

    webdav = webdav_dcache.WebDav()

    files_online = 0
    for surl in surls:
        status = webdav.locality(gridtoolslite.convert_to_webdav(surl))
        if status in {"ONLINE_AND_NEARLINE", "ONLINE"}:
            files_online += 1

    return files_online


# TODO Rename this here and in `check_staged_tokens`
def _extracted_from_check_staged_tokens_28(surls, doc, db):
    print(
        f"not all files({n_files_online}/{len(surls)}) online:{doc['_id']} reseting token"
    )
    doc["stage_lock"] = 0
    doc["stage_done"] = 0
    doc["lock"] = 0
    doc["done"] = 0

    db.update([doc])


def pin_files(surls, pintime=10800, gfalcontext=None):
    """
    use a list of surls to pin for a period (by default3 hours).
    this prevents files being purged from disk while waiting to be downloaded
    """
    # for surl in surls:
    if gfalcontext is None:
        gfalcontext = gfal2.creat_context()
    try:
        # bring_online(surl, pintime, timeout, async)
        (status, token) = gfalcontext.bring_online(surls, pintime, pintime, False)
        while status == 0:
            status = gfalcontext.bring_online_poll(surls, token)
    except gfal2.GError as e:
        print("Could not bring the file online:")
        print("\t", e.message)
        print("\t Code", e.code)


def check_token_online(doc):
    """

    :param doc:
    :return:
    """
    logger = logging.getLogger(__name__)

    logger.debug(doc)
    surls = extract_surls_from_token(doc)
    if n_files_online(surls) == len(surls):

        logger.debug(f"all {len(surls)} files online of token: " + str(doc["_id"]))

        return True
    else:
        logger.debug(str(doc["_id"]) + "not online")
        return False


def extract_surls_from_token(doc):
    files_remote_map = doc["files"]
    turls = [str(files_remote_map[filetype]["url"]) for filetype in files_remote_map]

    m = re.compile(".*/pnfs")
    return [
        m.sub("srm://srm.grid.sara.nl:8443/srm/managerv2?SFN=/pnfs", k) for k in turls
    ]


def reset_doc_values(doc, gfalcontext):
    """
    Reset a document of locked token to prestine "wait_to_stage" state and adds the scrub_count to account amount of fails
    This function returns the document but saves not to CouchDB
    """
    if "scrub_count" in doc:
        doc["scrub_count"] += 1
    else:
        doc["scrub_count"] = 1
    doc["lock"] = 0
    doc["hostname"] = ""
    if "log" in doc:
        del doc["log"]
    if "error" in doc:
        del doc["error"]

    doc["done"] = 0
    # delete all attachments if present
    if "_attachments" in doc:
        del doc["_attachments"]
    if "wms_job_id" in doc:
        del doc["wms_job_id"]

    if "stage_done" in doc:
        files_online = False
        try:
            files_online = check_token_online(doc)
        except FileNotFoundError as e:
            doc["lock"] = -1
            doc["done"] = -1
            print(f"file not found {e} in token {doc['_id']}")

        if files_online:
            doc["stage_done"] = int(time())
            doc["stage_lock"] = int(time())
            pin_files(extract_surls_from_token(doc), gfalcontext=gfalcontext)
        else:
            doc["stage_done"] = 0
            doc["stage_lock"] = 0


def wait2stage_to_stage(n=100):
    """
    Converts wait2stage tokens to to_stage state, so in next iteration of the stage script the files can be staged
    """
    # logger = logging.getLogger(__name__)

    db = gridtools.connect_to_couchdb(
        gridtools.credentials.URL,
        gridtools.credentials.USERNAME,
        gridtools.credentials.PASS,
        gridtools.credentials.DBNAME,
    )
    to_update = []

    i = 0
    for row in db.iterview(f"{gridtools.credentials.VIEW_NAME}/wait_to_stage", 50):
        if i >= n:
            break

        doc = row["value"]
        doc["stage_lock"] = 0
        # delete all attachments if present
        to_update.append(doc)
        i += 1
    db.update(to_update)
    logging.debug(f"converted {i} tokens from wait2stage to stage")


def create_views(viewname=None, picasdb=None):
    """
    creates the views in couchdb on basis of credentials in credentials.py
    this script should only run ones
    """

    if picasdb is None:
        picasdb = gridtools.connect_to_couchdb()
    if viewname is None:
        viewname = gridtools.credentials.VIEW_NAME
    view_js = {
        "language": "javascript",
        "version": 2,
        "views": {
            "overview": {
                "map": 'function (doc) {    if (doc.type==="TYPE"){      if (doc.stage_lock<0 && doc.stage_done==0 && doc.lock==0 && doc.done==0) {emit("1wait_to_stage", 1);}            if (doc.stage_lock==0 && doc.stage_done==0 && doc.lock==0 && doc.done==0) {emit("2to_stage", 1);}  if (doc.stage_lock>0 && doc.stage_done==0 && doc.lock==0 && doc.done==0) {emit("3staging", 1);}            if (doc.stage_lock>0 && doc.stage_done>0 && doc.lock==0 && doc.done==0) {emit("4todo", 1);}                 if(doc.lock>0 && doc.done==0) {emit("5locked", 1);}              if ( doc.lock>0 && doc.done>0) {emit("6done", 1);} if ( doc.lock==-1 && doc.done==-1) {emit("error", 1);}       }       }',
                "reduce": "_sum",
            },
            "to_stage": {
                "map": 'function (doc) { if (doc.type=="TYPE"){if (doc.stage_lock==0 && doc.stage_done==0 && doc.lock==0 && doc.done==0) {emit(doc._id, doc);}}}'
            },
            "todo": {
                "map": 'function (doc) { if (doc.type=="TYPE"){if (doc.stage_lock>0 && doc.stage_done>0 && doc.lock==0 && doc.done==0) {emit([doc.requirements.threads,doc.requirements.memory], doc);}}}',
                "reduce": "_count",
            },
            "locked": {
                "map": 'function (doc) { if (doc.type=="TYPE"){if (doc.lock>0 && doc.done==0) {emit(doc._id, doc);}}}'
            },
            "done": {
                "map": 'function (doc) { if (doc.type=="TYPE"){if (doc.stage_lock>0 && doc.stage_done>0 && doc.lock>0 && doc.done>0) {emit(doc._id, doc);}}}',
            },
            "staging": {
                "map": 'function (doc) { if (doc.type=="TYPE"){if (doc.stage_lock>0 && doc.stage_done==0 && doc.lock==0 && doc.done==0) {emit(doc._id, doc);}}}'
            },
            "wait_to_stage": {
                "map": 'function (doc) { if (doc.type=="TYPE"){if (doc.stage_lock<0 && doc.stage_done==0 && doc.lock==0 && doc.done==0) {emit(doc._id, doc);}}}'
            },
            "error": {
                "map": 'function (doc) { if (doc.type=="TYPE"){if ( doc.lock==-1 && doc.done==-1) {emit(doc._id, doc);}}}'
            },
        },
    }

    for key in view_js["views"].keys():
        view_js["views"][key]["map"] = view_js["views"][key]["map"].replace(
            "TYPE", viewname
        )

    if f"_design/{viewname}" not in picasdb:
        picasdb[f"_design/{viewname}"] = view_js
        print("uploaded new overview to DB")
    else:
        oldview = picasdb[f"_design/{viewname}"]
        view_js["_rev"] = oldview["_rev"]
        try:
            if oldview["version"] != view_js["version"]:
                print("CouchDB view is updated")
                picasdb[f"_design/{viewname}"] = view_js
        except KeyError:
            print("view not found")
            picasdb[f"_design/{viewname}"] = view_js


def overview_token_states():
    """

    :return:
    """
    db = gridtools.connect_to_couchdb()
    overview_raw = db.view(f"{gridtools.credentials.VIEW_NAME}/overview", group_level=1)

    return {a.key: int(a.value) for a in overview_raw.rows if isinstance(a.key, str)}


def totaljobs_to_proccess():
    """

    :return:
    """
    token_states = overview_token_states()
    waiting_states = [
        "4todo",
        "1wait2stage",
        "2tostage",
        "3staging",
        "todo",
        "wait2stage",
        "staging",
        "to_stage",
    ]
    return sum(token_states[state] for state in waiting_states if state in token_states)


def docs_in_view(view, viewname=gridtools.credentials.VIEW_NAME):
    """
    return amount of docs in a view
    """
    db = gridtools.connect_to_couchdb()
    return db.view(f"{viewname}/{view}", reduce=False).total_rows


def docs_in_todoview(view, viewname=gridtools.credentials.VIEW_NAME):
    """
    return amount of docs in a view
    """

    db = gridtools.connect_to_couchdb()
    dbiterator = db.iterview(f"{viewname}/{view}", 200, group=True, group_level=2)
    docs_overview = []
    for doc in dbiterator:
        r = {"threads": doc.key[0], "memory": doc.key[1], "n": doc.value}
        docs_overview.append(r)

    return docs_overview


def requirementgen(a):
    shuffle(a)
    for req in a:
        print(req)
        while req["n"] > 0:
            req["n"] -= 1
            yield ((req["threads"], req["memory"]))


def save_docs_in_db(
    docs,
    viewname=gridtools.credentials.VIEW_NAME,
    postfix=gridtools.credentials.VIEW_NAME,
):
    """
    Saves documents to CouchDB defined in credentials.py
    If document is present a message will be print and old document is kept unchanged
    """

    db = gridtools.connect_to_couchdb()
    for token in docs:
        token["sampleid"] = token["_id"]
        token["_id"] = token["_id"] + str(postfix)

        token["type"] = viewname
        if "_rev" in token:
            del token["_rev"]
        try:
            db.save(token)
        except gridtools.couchdb.ResourceConflict:
            print(token["_id"] + "already in database")


def reset_locked_tokens(hour=48):
    """

    Reset  locked tokens  older then hour(defauls =48) in view to prestine "wait_to_stage" state and adds the
    scrub_count to account amount of fails
    """
    db = gridtools.connect_to_couchdb()
    context = gfal2.creat_context()
    max_age = time() - (hour * 3600)
    to_update = []
    for row in db.iterview(f"{gridtools.credentials.VIEW_NAME}/locked", 100):
        doc = row["value"]
        if doc["lock"] < max_age:
            reset_doc_values(doc, gfalcontext=context)
            to_update.append(doc)

    db.update(to_update)
