import errno
import logging
import os
import re
import stat
import sys

import gfal2

import pmgridtools.gridtoolslite as gridtoolslite
import pmgridtools.webdav_dcache as webdav_dcache

logger = logging.getLogger("mylogger")


class StageRoadie:
    def __init__(self, pintime, stage_timeout):
        self.pin_time = pintime
        self.stage_timeout = stage_timeout
        self.surls = []
        self.stagetoken = None
        self.staging = False
        self.released_files = []
        self.all_staged = False
        self.ctx = gfal2.creat_context()

    def addsurl(self, surl):
        logger.debug(f"StageRoadie addsurl: surl {surl} self.surls: {self.surls}")
        if surl not in self.surls:
            self.surls.append(surl)

    def stagesurls(self):
        if self.staging:
            logger.error("already trying to stage files")
        else:
            self.staging = True
            logging.debug(f"sending stage command:{len(self.surls)}")

            (errors, self.stagetoken) = self.ctx.bring_online(
                self.surls, self.pin_time, self.stage_timeout, True
            )

            logging.debug("sending stage command done")

    def poll(self, only_just_released=False):
        """

        :param only_just_released: only return the surls that were released last poll (otherwise it returns a list with
        all surls)
        :return: list of surls
        """

        logging.debug(f"sending poll command.nsurls:{len(self.surls)}")
        just_released = []

        if self.stagetoken is None:
            logging.debug("staging again because stagetoken is not set")

            self.stagesurls()

        if self.stagetoken == "":
            logging.debug("staging done because stagetoken is set to empty string")

            just_released = self.surls
            self.released_files.extend(just_released)
            [self.surls.remove(surl) for surl in just_released]

        elif len(self.surls) > 0:
            logger.debug(f"stagetoken{self.stagetoken}")

            errors = self.ctx.bring_online_poll(self.surls, self.stagetoken)
            logging.debug("sending poll command done")
            just_released = self.poll_feedback_checker(errors)
            self.released_files.extend(just_released)
            [self.surls.remove(surl) for surl in just_released]
        else:
            logger.warning("All files staged")

        return just_released if only_just_released else self.released_files

    def poll_feedback_checker(self, errors):
        n_terminal = 0
        released_files = []
        for surl, error in zip(self.surls, errors):
            if error:
                if error.code != errno.EAGAIN:
                    logger.error(f"{surl} => FAILED: {error.message}")
                    n_terminal += 1
            else:
                n_terminal += 1
                released_files.append(surl)
                logger.debug(f"{surl} READY")

        return released_files


def get_pnfs(url: str) -> str:
    """

    :param url:
    :return:
    """
    url = url.strip()
    pnfs = None
    if url.startswith("gsiftp://") or url.startswith("srm://"):
        pnfs = re.sub(r".*/pnfs/grid.sara.nl/", "/pnfs/grid.sara.nl/", url)

    else:
        url = os.path.abspath(url)
        # print(url)
        if url.startswith("/projectmine-nfs/"):
            pnfs = url.replace(
                "/projectmine-nfs/", "/pnfs/grid.sara.nl/data/lsgrid/Project_MinE/"
            )
        else:
            print(
                f"invalled URL: only gsiftp:// , srm:// url or local paths in the /projectmine-nfs/ dir may be used: found the following:{url}"
            )

            sys.exit(1)

    assert pnfs is not None, "could not return an empty pnfs"
    return pnfs


def chop_list(large_list, size: int):
    """Generator to create small list of size size."""
    for i in range(0, len(large_list), size):
        yield large_list[i : i + size]


def bulk_remove(pnfs_list):
    """

    :param pnfs_list:
    :return:
    """
    webdavs = [gridtoolslite.convert_to_webdav(pnfs) for pnfs in pnfs_list]
    ctx = webdav_dcache.WebDav()
    [ctx.remove(url) for url in webdavs]


def remove(pnfs, recursively=False):
    """ """
    surl = f"srm://srm.grid.sara.nl:8443{pnfs}"
    ctx = gfal2.creat_context()
    try:
        stats = ctx.stat(surl)
    except gfal2.GError as e:
        print(f"{gfal2.GError}\t{e}")

        raise

    if stat.S_ISDIR(stats.st_mode):
        if recursively:
            removedir(pnfs)
        else:
            raise Exception("found directory while not deleting recursively")

    else:
        try:
            ctx.unlink(surl)
        except gfal2.GError as e:
            print(f"{gfal2.GError}\t{e}")
            raise


def removedir(pnfs):
    """
    Remove a directory recursively
    """
    # Content
    ctx = gfal2.creat_context()

    if pnfs[-1] != "/":
        pnfs += "/"

    surl = f"srm://srm.grid.sara.nl:8443{pnfs}"

    contents = ctx.listdir(surl)
    for c in contents:
        if c in [".", ".."]:
            continue
        remove(pnfs + c, recursively=True)

    try:
        ctx.rmdir(surl)
    except gfal2.GError as e:
        print(f"{gfal2.GError}\t{e}")
        raise
