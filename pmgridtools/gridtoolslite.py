import hashlib
import logging
import os
import re
import shlex
import sys
from shutil import which
from subprocess import PIPE, Popen

try:
    from deflate import adler32
except ImportError:
    from zlib import adler32


logger = logging.getLogger("mylogger")


def md5_of_file(filepath):
    """
    Calculate md5 checksum of filepath
    """
    afile = open(filepath, "rb")
    hasher = hashlib.md5()
    blocksize = 65536
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher.hexdigest()


def adler32_of_file(filepath):
    """
    Calculate adler32 checksum of filepath

    """
    BLOCKSIZE = 1048576  # that's 1 MB

    asum = 1
    with open(filepath, "rb") as f:
        while True:
            data = f.read(BLOCKSIZE)
            if not data:
                break
            asum = adler32(data, asum)
            if asum < 0:
                asum += 2**32

    return hex(asum)[2:10].zfill(8).lower()


def execute(args, shell=False, ignore_returncode=False, timing=True):
    """Helper function to more easily execute applications.
    @param shell: the arguments as they need to be specified for Popen.
    @return: a tuple containing the exitcode, stdout & stderr
    :param ignore_returncode:
    :param timing: time program with /bin/time
    :param shell:

    """

    if isinstance(args, list):
        logger.debug(" ".join(args))
    else:
        logger.debug(args)
    timingfile = ".gridtimings.tmp"
    if timing:
        timingcode = f"/bin/time -o {timingfile}"
        if isinstance(args, list):
            args = timingcode.split() + args
        else:
            args = f"{timingcode} {args}"
            args = shlex.split(args)

    try:
        proc = Popen(args, stdout=PIPE, stderr=PIPE, shell=shell)
    except OSError as e:
        logger.critical(e)
        logger.critical(args)
        exit(1)
    except Exception as e:
        e = sys.exc_info()[0]
        print(e)
        logger.error("Failed execute")
        logger.error(args)
        exit(1)
    (stdout, stderr) = proc.communicate()
    # make sure that type is not a number
    if type(stdout) == bytes:
        stdout = stdout.decode("utf-8")
    else:
        str(stdout)

    if type(stderr) == bytes:
        stderr = stderr.decode("utf-8")
    else:
        str(stderr)
    timingstats = None
    if timing:
        try:
            timingstats = open(timingfile, "rt").readlines()
            os.remove(timingfile)
        except FileNotFoundError:
            logger.warning("/bin/time output file not found")

    if not ignore_returncode and proc.returncode != 0:
        logger.error(f"exit code not 0: {str(proc.returncode)}")
        logger.error(" ".join(args))
        logger.error(f"stdout:{stdout}")
        logger.error(f"stderr:{stderr}")

        sys.exit(1)
    return {
        "exitcode": proc.returncode,
        "stdout": stdout,
        "stderr": stderr,
        "timing": timingstats,
    }


def validproxy():
    """

    :return: checks if proxy if valid. If not quit program and print help message
    """
    if which("voms-proxy-info") is None:
        print("voms-proxy-info not found: continue without checking")
        return True
    cmd = ["voms-proxy-info", "-e"]
    job_report = execute(cmd, ignore_returncode=True, timing=False)

    exitcode = int(job_report["exitcode"])
    if exitcode == 0:
        return True
    print(
        "Your proxy is not valid.\n \
        Inspect with 'voms-proxy-info -all' or regenerate with dirac-proxy-init -g projectmine.com_user -b 2048 -M  "
    )
    exit()


def convert_to_webdav(url):
    """
    Convert a turl/surl to a webdav url
    """
    url = get_pnfs(url)
    return re.sub(
        r".*/pnfs/grid.sara.nl/",
        "https://webdav.grid.surfsara.nl:2884/pnfs/grid.sara.nl/",
        url,
    )


def get_pnfs(url: str) -> str:
    """

    :param url:
    :return:
    """
    url = url.strip()
    if url.startswith("/pnfs/"):
        return url

    pnfs = None
    if url.startswith("gsiftp://") or url.startswith("srm://"):
        pnfs = re.sub(r".*/pnfs/grid.sara.nl/", "/pnfs/grid.sara.nl/", url)

    elif url.startswith("/projectmine-nfs/"):
        pnfs = url.replace(
            "/projectmine-nfs/", "/pnfs/grid.sara.nl/data/lsgrid/Project_MinE/"
        )
    else:
        print(
            f"invalled URL: only gsiftp:// , srm:// url or local paths in the /projectmine-nfs/ dir may be used: found the following:{url}"
        )
        sys.exit(1)

    assert pnfs is not None, "could not return an empty pnfs"
    return pnfs
