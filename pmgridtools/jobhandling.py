import logging
import tempfile
from shutil import which
from typing import Any, Dict, List

import pmgridtools.gridtools as gridtools
from pmgridtools.lib_stage import (  # check_jobs_in_queue,
    docs_in_todoview,
    docs_in_view,
    requirementgen,
)


def autosubmit_to_dirac(max_job_size, rcauth=True):
    """
    submits jobs if there are tokens in todo state.
    Number of jobs send is equal to tokens in "todo" state with a maximum of max_job_size

    """
    from DIRAC.Interfaces.API.Dirac import Dirac

    dirac = Dirac()
    logger = logging.getLogger(__name__)

    job_available = docs_in_view("todo", gridtools.credentials.VIEW_NAME)
    logger.info(f" jobs waiting: {job_available}")

    if job_available == 0:
        logger.info("\tno jobs available")
    else:
        # queued = check_jobs_in_queue(ce) - 1
        # todo fetch the number of queued jobs (now set a queued as fake value)
        queued = 0
        logger.info(f"queed jobs{queued}")
        jobstosubmit = max(0, job_available - queued)

        # set a maximum off amount of jobs to submit
        jobstosubmit = min(max_job_size, jobstosubmit)
        logger.debug(f"submitting  jobs:{jobstosubmit}")

        overview_jobs = docs_in_todoview("todo", gridtools.credentials.VIEW_NAME)
        reqgen = requirementgen(overview_jobs)
        for _ in range(jobstosubmit):
            try:
                threads, mem = next(reqgen)
            except StopIteration:
                logger.info("Could not generate more jobs")
                break
            # mem_mb = int(1024 * mem)
            # cputime = int(walltime * 60 * threads)
            # format the jdl
            if threads not in [1, 2, 4, 8]:
                logger.warning(
                    "Dirac only supports jobs with 1,2,4 or 8 cores. When requested another number of threads(now:{threads}), an higher tier of threads will be chosen(and also billed)"
                )
            jdlfile = f"""
[
Type = "Job";
JobName = "my-mine-job";
Executable = "/bin/bash";
Arguments = "/cvmfs/softdrive.nl/projectmine_sw/software/envs/dirac/setup_grid.sh {threads} {mem}";
ShallowRetryCount = 0;
RetryCount = 0;
StdError = "stderr.log";
StdOutput = "stdout.log";
OutputSandbox = {{"stdout.log", "stderr.log"}};
InputSandbox = {{"credentials.py"}};
OutputSandboxBaseDestURI = "gsiftp://localhost";
NumberOfProcessors = "{threads}";
Tags = {{"long"}};
CPUTime = "345600";
]
"""
            with tempfile.NamedTemporaryFile(
                mode="w+t", suffix=".jdl", delete=True
            ) as y:
                y.write(jdlfile)
                y.flush()

                result = dirac.submitJob(jdlfile)
                logger.debug(jdlfile)
                logger.debug((result))
        logger.info(f"submitting  jobs:{jobstosubmit}")


def check_jobs_in_queue_slurm():
    """

    :return:
    """

    cmd = "squeue -h -n bash -u $USER -t PD |wc -l"
    res = gridtools.execute(cmd, shell=True)
    # TODO: the stdout contains a binary wrapped as a string. execute needs to return a clean str
    a = res["stdout"].rstrip("\\n'").lstrip("b'")
    return int(a)


def autosubmit_to_slurm(max_job_size, queue="medium"):
    """
    submits jobs if there are tokens in todo state.
    Number of jobs send is equal to tokens in "todo" state with a maximum of max_job_size

    """
    logger = logging.getLogger(__name__)

    job_available = docs_in_view("todo", gridtools.credentials.VIEW_NAME)
    logger.info(f" jobs waiting: {job_available}")
    if "short" in queue:
        walltime = 4
    elif "medium" in queue:
        walltime = 36
    elif "long" in queue:
        walltime = 96

    if job_available == 0:
        logger.info("\tno jobs available")
    else:
        queued = check_jobs_in_queue_slurm()
        print(queued)
        logger.info(f"queed jobs{queued}")
        jobstosubmit = max(0, job_available - queued)

        # set a maximum off amount of jobs to submit
        jobstosubmit = min(max_job_size, jobstosubmit)
        logger.debug(f"submitting  jobs:{jobstosubmit}")

        overview_jobs = docs_in_todoview("todo", gridtools.credentials.VIEW_NAME)
        reqgen = requirementgen(overview_jobs)
        for _ in range(jobstosubmit):
            try:
                threads, mem = next(reqgen)
            except StopIteration:
                logger.info("Could not generate more jobs")
                break
            # mem_mb = int(1024 * mem)
            # cputime = int(walltime * 60 * threads)
            # format the jdl
            jdlfile = f"""#!/bin/bash
#SBATCH -N 1
#SBATCH --job-name=pm_job
#SBATCH -c {threads}
#SBATCH -t {walltime}:00:00

cp credentials.py $TMPDIR
cd $TMPDIR
/cvmfs/softdrive.nl/projectmine_sw/software/envs/dirac/setup_grid.sh {threads} {mem}
"""
            # print(jdlfile)
            with tempfile.NamedTemporaryFile(
                mode="w+t", suffix=".sbatch", delete=True
            ) as y:
                y.write(jdlfile)
                y.flush()

                cmd = f"sbatch {y.name}".split()
                logger.debug(cmd)

                res = gridtools.execute(cmd, timing=False)
                logger.debug(jdlfile)
                logger.debug(str(res))
        logger.info(f"submitting  jobs:{jobstosubmit}")


# noinspection PyUnresolvedReferences
class StopRunningJobs:
    def __init__(self) -> None:
        self.slurm_job_ids: List[str] = []
        self.dirac_job_ids: List[str] = []

    def add_dirac_job(self, dirac_id: str) -> None:
        """

        :param url:
        :return:
        """
        self.dirac_job_ids.append(dirac_id)

    def add_slurm_job(self, jobid: str) -> None:
        """

        :param url:
        :return:
        """
        self.slurm_job_ids.append(jobid)

    def addjobfromtoken(self, token: Dict[str, Any]) -> None:
        """

        :param token:
        :return:
        """
        diracfield = "dirac_job_id"
        if diracfield in token:
            self.add_dirac_job(token[diracfield])
        slurmfield = "slurm_job_id"
        if slurmfield in token:
            self.add_slurm_job(token[slurmfield])

    def stop_jobs(self) -> None:
        """

        :return:
        """
        # create a temp file for creamceids

        if not self.dirac_job_ids:
            from DIRAC.Interfaces.API.Dirac import Dirac

            result = Dirac().killJob(self.dirac_job_ids)
            if not result["OK"]:
                logging.error(f"Could not kill jobs{result['Message']}")

        if not self.slurm_job_ids:
            # check if sbatch is installed
            if which("sbatch") is not None:
                jobids = " ".join(self.slurm_job_ids)
                c = gridtools.execute(f"scancel {jobids}".split())
                print(c)
                self.slurm_job_ids = []
            else:
                logging.warning(
                    "scancel is not installed on this system and could not stop jobs"
                )
