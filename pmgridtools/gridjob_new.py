#!/usr/bin/env python3
# encoding: utf-8
import argparse
import glob
import logging
import os
import re
import sys

sys.path.append(os.getcwd())

import credentials  # noqa: E402
from picas.clients import CouchDB  # noqa: E402
from picas.iterators import TaskViewIterator  # noqa: E402
from picas.modifiers import BasicTokenModifier  # noqa: E402

import pmgridtools.gridtools as gridtools  # noqa: E402

"""

Gridjob -- shortdesc

Boiler plate code to run Project Mine Jobs.

@author:     Maarten Kooyman and Joke van Vugt

@copyright:  2015-2017 SURFsara. All rights reserved.

@license:    Apache 2.0

@contact:    projectmine@SURFsara.nl or j.f.a.vanvugt-2@umcutrecht.nl

"""

TMPDIR = os.environ["TMPDIR"] + "/"

REMOTE_BASE_URL = "srm://srm.grid.sara.nl/pnfs/grid.sara.nl/data/lsgrid/Project_MinE/"

_log_fmt = "%(asctime)s\t%(levelname)s\t%(lineno)d\t%(message)s"
_log_date_fmt = "%Y/%m/%d %H:%M:%S"
logger = logging.getLogger("mylogger")
logger.setLevel(logging.INFO)
formatter = logging.Formatter(fmt=_log_fmt, datefmt=_log_date_fmt)
ch = logging.StreamHandler()
ch.setFormatter(formatter)
logger.addHandler(ch)


class Bamlet(gridtools.MineRunner):
    def worker(self, token):

        filemapping = self.download_all_files(token["files"])
        sample = token["sampleid"]
        requiredfields = {}
        optionalfields = {"sample": sample}
        # Define bam values
        if token["format"] == "cram":
            bam_path = filemapping["cram"]
            # should be crai
            # bam_index_path = filemapping["crai"]
            requiredfields = {"bam_path": bam_path}

        elif token["format"] == "bam":
            # bam_index_path = filemapping["bai"]
            bam_path = filemapping["bam"]
            requiredfields = {"bam_path": bam_path}
        elif token["format"] == "random":
            print(filemapping)
            for k, v in filemapping.items():
                optionalfields[k] = v

        oldfiles = set(glob.glob("**", recursive=True))
        # substitute variables for the actual values
        cmd = gridtools.add_variables_to_bash(
            token["bash"], requiredfields, optionalfields
        )
        # run script as a bash script
        job_report = self.execute(gridtools.run_as_bash(cmd))
        # find new files
        newfiles = list(set(glob.glob("**", recursive=True)).difference(oldfiles))
        outfilter = token["filter"] if "filter" in token else ".*"
        r = re.compile(outfilter)

        newlist = list(filter(r.search, newfiles))
        # test for paths in newlist
        for newfile in newlist:
            if os.path.isdir(newfile) and len(os.listdir(newfile)) == 0:
                pass

        for file2upload in newlist:
            gridtools.upload_file(
                file2upload,
                f'{token["output"]}{sample}_{token["fingerprint"]}/{file2upload}',
            )
        return job_report


def main():
    parser = argparse.ArgumentParser(description="")

    parser.add_argument(
        "-t",
        "--threads",
        dest="threads",
        type=int,
        default=1,
        help="Amount of threads/cores program uses",
    )

    parser.add_argument(
        "-m", "--memory", dest="memory", type=int, default=8, help="Memory needed in GB"
    )

    parse_results = parser.parse_args()

    client = CouchDB(
        url=credentials.URL,
        username=credentials.USERNAME,
        password=credentials.PASS,
        db=credentials.DBNAME,
    )

    iterator = TaskViewIterator(
        client,
        "todo",
        design_doc="generic",
        key=[parse_results.threads, parse_results.memory],
        reduce=False,
    )
    modifier = BasicTokenModifier()
    actor = Bamlet(iterator, modifier)

    # set maximum time to process task in current job
    max_time = int(24 * 3600)

    # start processing
    actor.run(maxtime=max_time)


if __name__ == "__main__":
    main()
