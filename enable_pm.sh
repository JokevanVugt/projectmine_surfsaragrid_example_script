#!/bin/bash
if grep -P "^[A-z0-9 ].* PATH=.*/cvmfs/" "/home/${USER}/.bashrc" ;then
    echo "path found in ~/.bashrc that points to softdrive. PATH not added to ~/bashrc. Please remove or comment out the line in your .bashrc and try again"
else
    echo "appending PATH it now to ~/.bashrc"
    echo "export PATH=/cvmfs/softdrive.nl/projectmine_sw/software/bin:\$PATH"  >> ~/.bashrc
    echo "export REF_PATH=/cvmfs/softdrive.nl/projectmine_sw/resources/refpath/cache/%2s/%2s/%s:http://www.ebi.ac.uk/ena/cram/md5/%s" >> ~/.bashrc
    echo "please logout and login again to make the new programs availble"


fi
