On this page you will find information on getting access to the Project Mine data on the SURFsara grid, as well as processing the data at SURFsara grid site.

##New to the concept of grid?
The full documentation of grid at SURFsara can be found here - [SURFsara grid website](http://docs.surfsaralabs.nl/projects/grid/en/latest/index.html). This is an excellent starting point for beginners and we strongly recommend you to read it.
You may also follow video lectures at [MOOC](http://doc.grid.surfsara.nl/en/latest/Pages/Tutorials/MOOC/mooc.html#grid-computing-overview). On this page, we have selected a few pointers to get started, but please refer to the full documentation for all details.

##Getting started
To get you up and running you need to:

A. Get a grid certificate (User task)
B. Send a formal request  (Joke  van Vugt + SURFsara task)
C. Sign up for the Virtual Organisation (User task)
D. Access to the data (SURFsara task)

##A. Getting a grid certificate
You need a personal X509 grid certificate to access the grid. If you are unfamiliar with grid certificates we recommend you to go to this [link](http://doc.grid.surfsara.nl/en/latest/Pages/Tutorials/MOOC/mooc.html#grid-certificate-security).
There are different ways to get a grid certificate depending on your institution and geographical location. Each country may have a central Certificate Authority (CA) or multiple CAs, and the CAs may also support Universities from different countries. One of the following is applicable to you, so please follow the instructions:

1.[Digicert Certificate](http://doc.grid.surfsara.nl/en/latest/Pages/Basics/certificates/digicert.html) - In case that your institute supports SURFconext, DigiCert CA allows you to get your Grid certificate instantly from the GEANT Trusted Certificate Service. To check this:

   * Open a Firefox browser on your laptop
   * Access the [DigiCert portal](https://digicert.com/sso)
   * Start typing your institution/university name and select your institution from the list and login with your account.
   * Request a Grid certificate. Select: Product: ``Grid Premium``
   * If your browser prompts you to choose for which purposes to trust the CA, press �Cancel� in the window that pops up.
   * Once finished, you will have a Grid certificate automatically stored in your browser.
   * Please proceed to step B

2.[Dutch grid certificate](http://doc.grid.surfsara.nl/en/latest/Pages/Basics/certificates/dutchgrid.html) - If you do not find your institution listed in the above step, and you can visit SURFsara at Science Park in Amsterdam for identity verification, please follow the steps below:

   * Open a terminal, download the jdgridstart tool and run the wizard:
```
#!bash
curl -OL https://ca.dutchgrid.nl/start/jgridstart-wrapper-1.18.jar
java -jar jgridstart-wrapper-1.18.jar
```
   * Start the Wizard by pressing ``Request new ..`` button
   * Generate request by entering your details (name, surname, email, organisation). At this stage you will provide the password for your Grid certificate - make sure you keep this safe!
   * Submit request. This will create your private ``userkey.pem`` file in your ``~/.globus`` directory.
   * Fill in and print the verification form by pressing the ``display form`` button. Once you fill in the form, save it locally and close the wizard.
   * Check your details in the printed form. The Registration Authority at SURFsara will check your identity (ID, passport or driver's license) and sign the printed form. This verification at SURFsara should be performed within a month of requesting the certificate with the jdgridtart tool.
   * Please proceed to step B

3.If your institute does not support SURFconext and you cannot visit SURFsara at Science park in Amsterdam, please contact your local Certificate Authority (CA) to get a personal X509 certificate. We do not provide support for obtaining certificates with non-Dutch CA. The user may locate the local CA on this [link](https://www.igtf.net/pmamap).
  In case you don't know your local CA, please contact your local ICT support to provide this information.


##B. Send a formal request
To interact with the grid to access your data and analyse it, you will need access to the following:
 i. Grid Project MinE User Interface: mine-ui.grid.sara.nl, which is related to the [grid UI](http://doc.grid.surfsara.nl/en/latest/Pages/Basics/prerequisites.html#get-a-user-interface-account) account, to interact with the grid
 ii. CouchDB account to access the [Picas token framework](http://doc.grid.surfsara.nl/en/latest/Pages/Practices/pilot_jobs.html)
 iii. Read access to the couchhDB nosql Project MinE token [database](https://picas.surfsara.nl:6984/_utils/database.html?project_mine_tokens)
 iv. A personal database on couchDB
 v. [Softdrive](http://doc.grid.surfsara.nl/en/latest/Pages/Advanced/grid_software.html#softdrive) account

The user is recommended to read the documentation on the links provided to understand the purpose of each of the accounts.

To obtain the accounts above, you need to make a formal request through Project MinE's First Line support on SURFsara: Joke van Vugt (j.f.a.vanvugt-2@umcutrecht.nl) or Kristel van Eijk (k.vaneijk-2@umcutrecht.nl). Please provide the following information which will be forwarded to SURFsara:
1. Name:
2. Telephone number and email address:
3. Do you have a grid certificate?
  * If you followed step A.2 to obtain the Dutch grid certificate please indicate here. SURFsara will contact you to make an appointment for your visit to verify your identity and sign the certificate form
  * If you don't have a certificate yet, please go to step A
4. Indicate whether you already have a grid Project MinE UI account and if so, please provide your username

Once a formal request is received from Joke or Kristel and approved by Jan Veldink, SURFsara will provide the accounts to you and notify you to continue with step C..

##C. Sign up for the Virtual Organisation (VO) lsgrid and add SSH-key

1. A VO membership determines to which resources (compute and storage) you have access to. To familiarise yourself with the concept of a VO, you may visit this [link](http://doc.grid.surfsara.nl/en/latest/Pages/Tutorials/MOOC/mooc.html#virtual-organisations).
You are eligible to register for a VO only once you get a valid grid certificate. The file format of the certificate can be:
 * PKCS12 (provided by DigiCert) - this format is used by browsers
 * PEM (provided by DutchGrid) - this format is used by the Grid middleware and storage programs
 Follow the instructions [here](http://doc.grid.surfsara.nl/en/latest/Pages/Basics/prerequisites.html#join-a-virtual-organisation) to sign up for the VO lsgrid. Once you send the request to join the VO lsgrid, from the request in step B
SURFsara will already know that you are a valid user participating in Project Mine and will approve your request and add you to the group lsgrid/ProjectMine.

2. To be able to login to your MinE UI account, you should add an SSH-key to your [SURFsara portal account](https://portal.surfsara.nl/login/?next=/home/) as described [here](https://doc.hpccloud.surfsara.nl/SSHkey). You can login to the SURFsara portal using your Project MinE UI credentials.

3. You should install the grid certificate in PEM format on the Project MinE UI in the ``~/.globus`` directory to be able to interact with the grid (access data, processing) and the PKCS12 format on the Firefox browser on the UI (to go to the voms website and sign up for the VO).
You may also use your laptop browser for the VO sign up, however please note that SURFsara can provide support for troubleshooting only for the Firefox browser on the UI.
Depending on the format of your certificate, below are some pointers:
 * Download certificate from the browser - [export](http://doc.grid.surfsara.nl/en/latest/Pages/Basics/certificates/digicert.html#export-certificate-from-browser)
 * Upload certificate to the browser [import](http://doc.grid.surfsara.nl/en/latest/Pages/Basics/certificates/dutchgrid.html#import-the-certificate-to-the-browser)
 * Convert between certificate formats - [convert](http://doc.grid.surfsara.nl/en/latest/Pages/Advanced/grid_certificates.html#conversion-of-key-and-certificate-formats)
 * Copy files to the UI - e.g., browsercert.p12 to the UI [copy](http://doc.grid.surfsara.nl/en/latest/Pages/Basics/certificates/digicert.html#copy-certificate-p12-file-to-the-ui)

##D. Access to the data
Only when step C is completed SURFsara can proceed to provide you access to the Project Mine data. SURFsara will contact you once this step has been accomplished.

Now you are ready to start working on the grid. You may proceed to the [example script](https://bitbucket.org/JokevanVugt/projectmine_surfsaragrid_example_script).
